package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static ArrayList<Player> players = new ArrayList();

    public static void main(String[] args) {

        CardsStack cardStack = new CardsStack();
        MainPlayer mainPlayer = new MainPlayer();
        Player dealer = new Dealer();


        GameRules gameRules = new GameRules(mainPlayer,dealer,cardStack);
        gameRules.printTitle();
        gameRules.firstRoundOfCards(gameRules.choosePlayers(), players);
        gameRules.printInfo();
        gameRules.chooseAction();
        gameRules.playersAIPlay(players);
        gameRules.dealerPlay();
        gameRules.chooseWinner(players);

    }

}
