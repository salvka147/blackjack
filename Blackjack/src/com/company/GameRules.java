package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class GameRules {

    private MainPlayer mainPlayer;
    private Player dealer;
    private CardsStack cardsStack;
    private Scanner scanner = new Scanner(System.in);

    public GameRules(MainPlayer mainPlayer, Player dealer, CardsStack cardsStack) {
        this.mainPlayer = mainPlayer;
        this.dealer = dealer;
        this.cardsStack = cardsStack;
    }

    public int choosePlayers(){
        System.out.println("pasirinkite su kiek priesininku noretumete zaisti. zaidime dalyvauja dalintojas + nuo 1 iki 3 priesininku");
        while (true) {
            char n = scanner.next().charAt(0);
            switch (n) {
                case '1':
                    return 1;
                case '2':
                    return  2;
                case '3':
                    return  3;
                default:
                    System.out.println("Pasirinkite galima skaiciu zaideju");
                    break;
            }
        }
    }

    public void firstRoundOfCards(int playerCount, ArrayList<Player> players){
        for (int i = 1; i <= playerCount; i++) {
            players.add(new Player());
        }

        for (int i = 0; i < 2; i++) {
            mainPlayer.addCardToPlayer(cardsStack);
        }

        for (int i = 0; i < 2; i++) {
            dealer.addCardToPlayer(cardsStack);
        }

        for (Player p : players) {
            for (int i = 0; i < 2; i++) {
                p.addCardToPlayer(cardsStack);
            }
        }
    }

    public void printTitle(){
        System.out.println("--------------BLACKJACK--------------");
    }

    public void printInfo(){
        System.out.println("Jusu kortos:");
        mainPlayer.printCards();
        System.out.println("Jusu tasku skaicius:");
        System.out.println(mainPlayer.getPoints());
        System.out.println("");
        System.out.println("Dalintojo korta:");
        dealer.PrintFirstCard();
    }

    public void chooseAction(){
        System.out.println("pasirinkite koki veiksma norite atlikti. 's' - netraukti naujos kortos ir baigti ejima. 'h' - traukti nauja korta. 'q' - pasiduoti ir baigti zaidima");
        loop2:
        while (true) {
            char n = scanner.next().charAt(0);
            switch (n) {
                case 'h':
                    mainPlayer.addCardToPlayer(cardsStack);
                    printInfo();
                    mainPlayer.checkPoints(this);
                    break;
                case 's':
                    break loop2;
                case 'q':
                    endGame("jus pasidavete. ZAIDIMAS BAIGTAS");
                    break;
                default:
                    System.out.println("Pasirinkite galima veiksma");
                    break;
            }
        }

    }

    public void endGame(String text) {
        System.out.println(text);
        System.exit(0);

    }

    public void dealerPlay(){
        System.out.println("dalintojas atvercia savo antra korta");
        System.out.println("dalintojo kortos:");
        dealer.printCards();
        System.out.println("dalintojo tasku skaicius: " + dealer.getPoints());
        dealer.chooseToDrawCard(1, cardsStack);

        System.out.println("dalintojo kortos:");
        dealer.printCards();

        int dealerScore = dealer.getPoints();
        if (dealer.getPoints() > 21) {
            System.out.println("dalintojas virsijo tasku skaiciu. jam zaidimas baigtas");
            dealer.setPoints(0);
        }
    }

    public void chooseWinner(ArrayList<Player> players) {

        String winner = "";
        int winnerPoints = 0;
        int index = 1;
        for(Player p: players){
            if(p.getPoints() > 0) {
                System.out.println(index + "-ojo zaidejo tasku skaicius: " + p.getPoints());
                if(p.getPoints()> winnerPoints){
                    winnerPoints = p.getPoints();
                    winner = index + "-asis zaidejas";
                }
            }
            index++;
        }

        System.out.println("Jusu tasku skaicius: " + mainPlayer.getPoints());
        if(mainPlayer.getPoints() > winnerPoints){
            winnerPoints = mainPlayer.getPoints();
            winner = "JUS! Sveikiname";
        }

        if(dealer.getPoints()> 0){
            System.out.println("dalintojo tasku skaicius: " + dealer.getPoints());
            if(dealer.getPoints() > winnerPoints){
                winnerPoints = dealer.getPoints();
                winner = "dalintojas";
            }
        }

        System.out.println("nugaletojas: " + winner);
    }

    public void playersAIPlay(ArrayList<Player> players){
        int index = 1;
        for (Player p : players) {
            p.chooseToDrawCard(index, cardsStack);
            index++;
        }
    }

}
