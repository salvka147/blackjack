package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Player {

    protected ArrayList<String> cards = new ArrayList<String>();

    protected int points = 0;

    public List<String> getCards() {
        return cards;
    }

    public int getPoints() {
        return points;
    }

    public void addPoints(String card){

        String[] cardValues = card.split(" ");
        if( cardValues[0].equals("tuzas") && points+Integer.parseInt(cardValues[2]) < 21){
            cardValues[2] = "11";
        }
        points+= Integer.parseInt(cardValues[2]);

        cards.add(cardValues[0] + " " + cardValues[1]);
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void printCards(){
        for(String card:cards ){
            System.out.println(card);
        }
    }

    public void addCardToPlayer(CardsStack cardStack){
        int idx = new Random().nextInt(cardStack.getCardStack().length);
        String randomCard = (cardStack.getCardStack()[idx]);
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(cardStack.getCardStack()));
        list.remove(randomCard);
        cardStack.setCardStack(list.toArray(new String[0]));
        addPoints(randomCard);
    }

    public void chooseToDrawCard( int playerNumber, CardsStack cardsStack){
        if(getPoints() > 16){
            System.out.println(playerNumber + "-sis zaidejas netrauke naujos kortos");
        }
        else{
            while(getPoints() <= 16 ) {
                System.out.println(playerNumber + "-sis zaidejas traukia korta");
                addCardToPlayer(cardsStack);
                if(getPoints() > 21){
                    System.out.println(playerNumber + "-sis zaidejas virsijo tasku skaiciu. jam zaidimas baigtas");
                    setPoints(0);
                    break;
                }
            }
        }
    }
    public void PrintFirstCard(){
        System.out.println(cards.get(0));
    }
}
