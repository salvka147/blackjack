import com.company.CardsStack;
import com.company.Player;
import org.junit.Assert;
import org.junit.Test;

public class TestClass {

        @Test
        public void firstTest() {
            Player p = new Player();
            p.setPoints(9);
            p.addPoints("tuzas vynu 1");
            Assert.assertEquals(20, p.getPoints());
        }

        @Test
        public void SecondTest() {
            Player p = new Player();
            p.setPoints(10);
            p.addPoints("dvejetas vynu 2");
            Assert.assertEquals(12, p.getPoints());
        }

        @Test
        public void ThirdTest() {
            Player p = new Player();
            CardsStack c = new CardsStack();
            p.addCardToPlayer(c);
            Assert.assertEquals(1, p.getCards().size());
        }
}
