package com.company;

public class CardsStack {


    private String[] cardStack = new String[]{
            "tuzas vynu 1", "tuzas kryziu 1", "tuzas sirdziu 1", "tuzas bugnu 1",
            "dvejetas vynu 2", "dvejetas kryziu 2", "dvejetas sirdziu 2", "dvejetas bugnu 2",
            "trejetas vynu 3", "trejetas kryziu 3", "trejetas sirdziu 3", "trejetas bugnu 3",
            "ketvertas vynu 4", "ketvertas kryziu 4", "ketvertas sirdziu 4", "ketvertas bugnu 4",
            "penketas vynu 5", "penketas kryziu 5", "penketas sirdziu 5", "penketas bugnu 5",
            "sesetas vynu 6", "sesetas kryziu 6", "sesetas sirdziu 6", "sesetas bugnu 6",
            "septynetas vynu 7", "septynetas kryziu 7", "septynetas sirdziu 7", "septynetas bugnu 7",
            "astuonetas vynu 8", "astuonetas kryziu 8", "astuonetas sirdziu 8", "astuonetas bugnu 8",
            "devynetas vynu 9", "devynetas kryziu 9", "devynetas sirdziu 9", "devynetas bugnu 9",
            "desimt vynu 10", "desimt kryziu 10", "desimt sirdziu 10", "desimt bugnu 10",
            "bartukas vynu 10", "bartukas kryziu 10", "bartukas sirdziu 10", "bartukas bugnu 10",
            "dama vynu 10", "dama kryziu 10", "dama sirdziu 10", "dama bugnu 10",
            "karalius vynu 10", "karalius kryziu 10", "karalius sirdziu 10", "karalius bugnu 10"};

    public String[] getCardStack() {
        return cardStack;
    }

    public void setCardStack(String[] cardStack) {
        this.cardStack = cardStack;
    }
}

